
import os
from celery import Celery
from celery.schedules import crontab
from project import settings
from api.tasks import *




os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')


app = Celery('project')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda:settings.INSTALLED_APPS)
app.conf.beat_schedule={
    'test':{
        'task':'api.views.ress',
        'schedule':crontab(),
        
    }
}



@app.task()
def debug_task(self):
    print(f'Request: {self.request!r}')
 