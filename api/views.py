from rest_framework.decorators import api_view
from rest_framework.response import Response
import random
import uuid
import api
from api.models import *
import datetime
from datetime import date,time
from django.http import JsonResponse, response
from celery import shared_task
from django.core import serializers
import string
import urllib, json
import requests
from django.core.mail import send_mail 
from django.conf import settings
from django.shortcuts import render
import operator
# Create your views here.

@shared_task
def ress():
    start_date = datetime.date.today()
    end_date = start_date + datetime.timedelta(days=random.randint(1,5))
    res=list()
    for i in range(10):
        id=uuid.uuid4()
        newguests = list ()
        nbrguest=random.randint(1,5)
        nbrroom=random.randint(1,nbrguest)
        newrooms = list ()
        totals = random.randint(100,300)
        guest={
        "guest_id_pms": "da15c5dc-7411-47ff-a9ed-ad6c00e28b90",
        "title": "",
        "language": "EN",
        "first_name": "",
        "last_name": "Groupe Cimalpes 04-05 Octobre 2021",
        "email": "",
        "phone": "",
        "loyalty_code": "",
        "address1": "",
        "address2": "",
        "gender": "",
        "city": "",
        "passport_number": "",
        "passport_expire_date": None,
        "passport_issuance_contry_code": "ND",
        "passport_issuance_date": None,
        "state": "",
        "country": "ND",
        "nationality": "ND",
        "zip_code": "",
        "birthday": None,
        "document_type": None,
        "birthday_place": "",
        "note": "",
        "is_main_guest": True,
        "reservation_guest_data": {
            "start_date": start_date.strftime("%Y-%d-%m"),
            "end_date": end_date.strftime("%Y-%d-%m")
        },
        "reservation_id_pms": "550ca1c1-6ca3-4404-8340-ad6c00e30661"
        }
        for seq in range(nbrguest):
            newguests.append(guest)
        for seq in range(nbrroom):
            room={
        "room_id_pms": "f66af656-f2bd-49c9-a50f-531c2ba3d45b",
        "room_name": "460",
        "room_number": "460",
        "other_rooms_number": [
            
        ],
        "room_type_pms": "a5742d5f-ade5-45e2-80e0-cfaa246e0932",
        "room_type": "room",
        "room_floor": "4",
        "room_building": "MONT-BLANC",
        "room_category_name": typeroom(),
        "reservation_room_data": {
            "start_date": "2021-10-04",
            "end_date": "2021-10-05",
            "guests": [
            "da15c5dc-7411-47ff-a9ed-ad6c00e28b90",
            "2d991f3b-b230-468d-a0f3-adb4009bb065",
            "212448c7-db54-465d-a679-adb4009bb069",
            "df32d5b7-8385-416d-bc16-adb4009bb06d",
            "0b1a7ce2-aa15-4b21-9928-adb4009bb070"
            ],
            "number_of_keys": 2,
            "guest_id_pms": "da15c5dc-7411-47ff-a9ed-ad6c00e28b90",
            "guest_first_name": "",
            "guest_last_name": "Groupe Cimalpes 04-05 Octobre 2021",
            "number_of_children": 0,
            "rates": [
            
            ],
            "balance": 0,
            "rate_id_pms": "62cf8b3f-7257-451f-8c21-ed80de8cd166",
            "is_available": False,
            "reservation_id_pms": "550ca1c1-6ca3-4404-8340-ad6c00e30661",
            "number_of_adults": 4,
            "room_total": 0
        
        }

        }
            newrooms.append(room)

        
        content={
                "customer": 48,
                "reservation_id_pms": str(id),
                "reservation_channel_number": None,
                "reservation_group_id_pms": "ed2b9d55-46d9-4471-a1e9-ad6c00e30661",
                "extra_reservation_code": "550ca1c1",
                "is_main_reservation": False,
                "start_date": "2021-10-04",
                "end_date": "2021-10-05",
                "arrival_time_estimated": "14:00:00",
                "departed_time_estimated": "09:00:00",
                "number_of_adults": 4,
                "number_of_keys": 2,
                "number_of_children": 0,
                "number_of_guests": 4,
                "number_of_night": 1,
                "travel_agency_id": None,
                "status": "Confirmed",
                "checked_in": False,
                "checked_out": False,
                "guests": newguests,
                "country_main_guest": "ND",
                "nationality_main_guest": "ND",
                "language_main_guets": "",
                "gender_main_guest": "",
                "first_name_main_guest": "",
                "last_name_main_guest": "Groupe Cimalpes 04-05 Octobre 2021",
                "email_main_guest": "",
                "title_main_guest": "",
                "main_guest_id": "da15c5dc-7411-47ff-a9ed-ad6c00e28b90",
                "number_of_rooms": 1,
                "rooms": newrooms,
                "main_room_number": "460",
                "total": totals,
                "balance": 0,
                "paid": 0,
                "invoice": "{}"
                
                }
        reservations=reservation.objects.create(res=content)
        reservations.save()
        res.append(content)
    return True

def typeroom():
    room=['Chambre simple','Chambre double','king']
    x=random.randint(0,2)
    return room[x]
def filtress(request):
    s=list()
    d=dict()
    reservations=reservation.objects.all()
    for i in reservations:
        x = json.dumps(i.res)
        k=json.loads(x) 
        s.append(k) 
    return JsonResponse(s,safe=False)
def ajout(request):
    if request.GET:
        condition = request.GET['condition']
        x=json.loads(condition)
        hotels= hotel.objects.create(res=x)
        hotels.save()
    return render(request, 'ajout.html', {})
def get_api(request):
    allowed_operators={
    "+": operator.add,
    "-": operator.sub,
    "*": operator.mul,
    "/": operator.truediv,
    ">": operator.gt,
    "<": operator.le,
    "==":operator.ne,
    }
    reservations = Reservation.objects.all()
    notifications = Notification.objects.all()
    list = []
    for res in reservations:
        id= res.res['reservation_id_pms']
        for notif in notifications:
            send = True
            for cond in notif.conditions:
                string_operator= cond['operation']
                attr = cond['field']
                value = cond['value']
                send = allowed_operators[string_operator](res.res[attr],value) and send
            send and send_mail_after_registration(id)
            send and list.append(res.res['reservation_id_pms'])            
    return JsonResponse(list, safe=False)
def send_mail_after_registration(id):
    subject = 'reservation'
    message = f'votre reservation est effectuée avec succes id :reservation : {id}'
    email_from = settings.EMAIL_HOST_USER
    recipient_list = ['aaydi@outlook.fr']   
    send_mail(subject, message , email_from ,recipient_list )



