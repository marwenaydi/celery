from django.db import models
from datetime import datetime 
import jsonfield

# Create your models here.
class Reservation(models.Model):
    res= jsonfield.JSONField()
    da = models.DateTimeField(default=datetime.now, blank=True)

class Notification(models.Model):
    name = models.CharField(max_length = 150, default="")
    conditions = jsonfield.JSONField()
    text = models.CharField(max_length = 150, default="")
    
    
